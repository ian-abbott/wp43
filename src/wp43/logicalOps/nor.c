// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 Authors

#include "logicalOps/nor.h"

#include "debug.h"
#include "error.h"
#include "items.h"
#include "registers.h"
#include "registerValueConversions.h"
#include "stack.h"

#include "wp43.h"

#if (EXTRA_INFO_ON_CALC_ERROR == 1)
  /**
   * Data type error in NOR
   */
  void norError24  (void);
  void norError31  (void);
#else // (EXTRA_INFO_ON_CALC_ERROR == 1)
  #define norError24 typeError
  #define norError31 typeError
#endif // (EXTRA_INFO_ON_CALC_ERROR == 1)

TO_QSPI void (* const logicalNor[NUMBER_OF_DATA_TYPES_FOR_CALCULATIONS][NUMBER_OF_DATA_TYPES_FOR_CALCULATIONS])(void) = {
// regX |    regY ==>   1            2            3           4           5           6           7           8            9             10
//      V               Long integer Real34       Complex34   Time        Date        String      Real34 mat  Complex34 m  Short integer Config data
/*  1 Long integer  */ {norLonILonI, norRealLonI, norError24, norError24, norError24, norError24, norError24, norError24,  norError31,   norError24},
/*  2 Real34        */ {norLonIReal, norRealReal, norError24, norError24, norError24, norError24, norError24, norError24,  norError31,   norError24},
/*  3 Complex34     */ {norError24,  norError24,  norError24, norError24, norError24, norError24, norError24, norError24,  norError24,   norError24},
/*  4 Time          */ {norError24,  norError24,  norError24, norError24, norError24, norError24, norError24, norError24,  norError24,   norError24},
/*  5 Date          */ {norError24,  norError24,  norError24, norError24, norError24, norError24, norError24, norError24,  norError24,   norError24},
/*  6 String        */ {norError24,  norError24,  norError24, norError24, norError24, norError24, norError24, norError24,  norError24,   norError24},
/*  7 Real34 mat    */ {norError24,  norError24,  norError24, norError24, norError24, norError24, norError24, norError24,  norError24,   norError24},
/*  8 Complex34 mat */ {norError24,  norError24,  norError24, norError24, norError24, norError24, norError24, norError24,  norError24,   norError24},
/*  9 Short integer */ {norError31,  norError31,  norError24, norError24, norError24, norError24, norError24, norError24,  norShoIShoI,  norError24},
/* 10 Config data   */ {norError24,  norError24,  norError24, norError24, norError24, norError24, norError24, norError24,  norError24,   norError24}
};

#if (EXTRA_INFO_ON_CALC_ERROR == 1)
  void norError24(void) {
    displayCalcErrorMessage(ERROR_INVALID_DATA_TYPE_FOR_OP, ERR_REGISTER_LINE, REGISTER_X);
    errorMoreInfo("%s NOR %s\ndata type of one of the NOR parameters is not allowed", getRegisterDataTypeName(REGISTER_Y, false, false), getRegisterDataTypeName(REGISTER_X, false, false));
  }



  void norError31(void) {
    displayCalcErrorMessage(ERROR_INVALID_DATA_TYPE_FOR_OP, ERR_REGISTER_LINE, REGISTER_X);
    errorMoreInfo("%s NOR %s\nNOR doesn't allow mixing data types real/long integer and short integer", getRegisterDataTypeName(REGISTER_Y, false, false), getRegisterDataTypeName(REGISTER_X, false, false));
  }
#endif // (EXTRA_INFO_ON_CALC_ERROR == 1)



void fnLogicalNor(uint16_t unusedButMandatoryParameter) {
  if(!saveLastX()) {
    return;
  }
  logicalNor[getRegisterDataType(REGISTER_X)][getRegisterDataType(REGISTER_Y)]();
  fnDropY(NOPARAM);
}



void norLonILonI(void) {
  longInteger_t x, res;

  convertLongIntegerRegisterToLongInteger(REGISTER_X, x);
  convertLongIntegerRegisterToLongInteger(REGISTER_Y, res);

  if(longIntegerIsZero(x) && longIntegerIsZero(res)) {
    uIntToLongInteger(1, res);
  }
  else {
    uIntToLongInteger(0, res);
  }

  convertLongIntegerToLongIntegerRegister(res, REGISTER_X);

  longIntegerFree(x);
  longIntegerFree(res);
}



void norLonIReal(void) {
  longInteger_t res;

  convertLongIntegerRegisterToLongInteger(REGISTER_Y, res);

  if(real34IsZero(REGISTER_REAL34_DATA(REGISTER_X)) && longIntegerIsZero(res)) {
    uIntToLongInteger(1, res);
  }
  else {
    uIntToLongInteger(0, res);
  }

  convertLongIntegerToLongIntegerRegister(res, REGISTER_X);

  longIntegerFree(res);
}



void norRealLonI(void) {
  longInteger_t res;

  convertLongIntegerRegisterToLongInteger(REGISTER_X, res);

  if(real34IsZero(REGISTER_REAL34_DATA(REGISTER_Y)) && longIntegerIsZero(res)) {
    uIntToLongInteger(1, res);
  }
  else {
    uIntToLongInteger(0, res);
  }

  convertLongIntegerToLongIntegerRegister(res, REGISTER_X);

  longIntegerFree(res);
}



void norRealReal(void) {
  longInteger_t res;

  longIntegerInit(res);
  if(real34IsZero(REGISTER_REAL34_DATA(REGISTER_X)) && real34IsZero(REGISTER_REAL34_DATA(REGISTER_Y))) {
    uIntToLongInteger(1, res);
  }
  else {
    uIntToLongInteger(0, res);
  }

  convertLongIntegerToLongIntegerRegister(res, REGISTER_X);

  longIntegerFree(res);
}



void norShoIShoI(void) {
  *(REGISTER_SHORT_INTEGER_DATA(REGISTER_X)) = ~(*(REGISTER_SHORT_INTEGER_DATA(REGISTER_X)) | *(REGISTER_SHORT_INTEGER_DATA(REGISTER_Y))) & shortIntegerMask;
  setRegisterShortIntegerBase(REGISTER_X, getRegisterShortIntegerBase(REGISTER_Y));
}
