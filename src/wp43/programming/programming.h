// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 Authors

/**
 * \file programming/programming.h
 */
#if !defined(PROGRAMMING_H)
  #define PROGRAMMING_H

  #include "clcvar.h"
  #include "decode.h"
  #include "input.h"
  #include "lblGtoXeq.h"
  #include "manage.h"
  #include "nextStep.h"
  #include "programmableMenu.h"

#endif // !PROGRAMMING_H
