// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 Authors

/**
 * \file solver/sumprod.h
 */
#if !defined(SUMPROD_H)
  #define SUMPROD_H

  #include <stdint.h>

  void fnProgrammableSum    (uint16_t label);
  void fnProgrammableProduct(uint16_t label);

#endif // !SUMPROD_H
