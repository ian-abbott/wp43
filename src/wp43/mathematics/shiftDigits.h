// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 Authors

/**
 * \file mathematics/shiftDigits.h
 */
#if !defined(SHIFTDIGITS_H)
  #define SHIFTDIGITS_H

  #include <stdint.h>

  void fnSdr(uint16_t numberOfShifts);
  void fnSdl(uint16_t numberOfShifts);

#endif // !SHIFTDIGITS_H
