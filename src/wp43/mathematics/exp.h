// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 Authors

/**
 * \file mathematics/exp.h
 */
#if !defined(EXP_H)
  #define EXP_H

  #include "defines.h"
  #include "realType.h"
  #include <stdint.h>

  void fnExp   (uint16_t unusedButMandatoryParameter);

  #if (EXTRA_INFO_ON_CALC_ERROR == 1)
    void expError(void);
  #else // (EXTRA_INFO_ON_CALC_ERROR != 1)
    #define expError typeError
  #endif // (EXTRA_INFO_ON_CALC_ERROR == 1)

  void expLonI (void);
  void expRema (void);
  void expCxma (void);
  void expShoI (void);
  void expReal (void);
  void expCplx (void);
  void expComplex(const real_t *real, const real_t *imag, real_t *resReal, real_t *resImag, realContext_t *realContext);
  void real34Exp (const real34_t *xin, real34_t *res);

#endif // !EXP_H
