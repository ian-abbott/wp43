// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 Authors

/**
 * \file mathematics/dblMultiplication.h
 */
#if !defined(DBLMULTIPLICATION_H)
  #define DBLMULTIPLICATION_H

  #include <stdint.h>

  void fnDblMultiply(uint16_t unusedButMandatoryParameter);

#endif // !DBLMULTIPLICATION_H
