// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 Authors

#include "mathematics/addition.h"

#include "charString.h"
#include "config.h"
#include "constantPointers.h"
#include "conversionAngles.h"
#include "debug.h"
#include "display.h"
#include "error.h"
#include "fonts.h"
#include "integers.h"
#include "items.h"
#include "mathematics/matrix.h"
#include "registers.h"
#include "registerValueConversions.h"
#include "rsd.h"
#include "stack.h"

#include "wp43.h"

void addLonILonI(void);
void addLonIRema(void);
void addLonICxma(void);
void addLonITime(void);
void addLonIDate(void);
void addLonIStri(void);
void addLonIShoI(void);
void addLonIReal(void);
void addLonICplx(void);
void addTimeLonI(void);
void addTimeTime(void);
void addTimeStri(void);
void addTimeReal(void);
void addDateLonI(void);
void addDateStri(void);
void addDateReal(void);
void addStriLonI(void);
void addStriTime(void);
void addStriStri(void);
void addStriRema(void);
void addStriCxma(void);
void addStriDate(void);
void addStriShoI(void);
void addStriReal(void);
void addStriCplx(void);
void addRemaLonI(void);
void addRemaRema(void);
void addRemaCxma(void);
void addRemaStri(void);
void addRemaShoI(void);
void addRemaReal(void);
void addRemaCplx(void);
void addCxmaLonI(void);
void addCxmaRema(void);
void addCxmaCxma(void);
void addCxmaStri(void);
void addCxmaShoI(void);
void addCxmaReal(void);
void addCxmaCplx(void);
void addShoILonI(void);
void addShoIRema(void);
void addShoICxma(void);
void addShoIStri(void);
void addShoIShoI(void);
void addShoIReal(void);
void addShoICplx(void);
void addRealLonI(void);
void addRealRema(void);
void addRealCxma(void);
void addRealTime(void);
void addRealDate(void);
void addRealStri(void);
void addRealShoI(void);
void addRealReal(void);
void addRealCplx(void);
void addCplxLonI(void);
void addCplxRema(void);
void addCplxCxma(void);
void addCplxStri(void);
void addCplxShoI(void);
void addCplxReal(void);
void addCplxCplx(void);

TO_QSPI void (* const addition[NUMBER_OF_DATA_TYPES_FOR_CALCULATIONS][NUMBER_OF_DATA_TYPES_FOR_CALCULATIONS])(void) = {
// regX |    regY ==>   1            2            3            4            5            6            7            8            9             10
//      V               Long integer Real34       Complex34    Time         Date         String       Real34 mat   Complex34 m  Short integer Config data
/*  1 Long integer  */ {addLonILonI, addRealLonI, addCplxLonI, addTimeLonI, addDateLonI, addStriLonI, addRemaLonI, addCxmaLonI, addShoILonI,  addError},
/*  2 Real34        */ {addLonIReal, addRealReal, addCplxReal, addTimeReal, addDateReal, addStriReal, addRemaReal, addCxmaReal, addShoIReal,  addError},
/*  3 Complex34     */ {addLonICplx, addRealCplx, addCplxCplx, addError,    addError,    addStriCplx, addRemaCplx, addCxmaCplx, addShoICplx,  addError},
/*  4 Time          */ {addLonITime, addRealTime, addError,    addTimeTime, addError,    addStriTime, addError,    addError,    addError,     addError},
/*  5 Date          */ {addLonIDate, addRealDate, addError,    addError,    addError,    addStriDate, addError,    addError,    addError,     addError},
/*  6 String        */ {addLonIStri, addRealStri, addCplxStri, addTimeStri, addDateStri, addStriStri, addRemaStri, addCxmaStri, addShoIStri,  addError},
/*  7 Real34 mat    */ {addLonIRema, addRealRema, addCplxRema, addError,    addError,    addStriRema, addRemaRema, addCxmaRema, addShoIRema,  addError},
/*  8 Complex34 mat */ {addLonICxma, addRealCxma, addCplxCxma, addError,    addError,    addStriCxma, addRemaCxma, addCxmaCxma, addShoICxma,  addError},
/*  9 Short integer */ {addLonIShoI, addRealShoI, addCplxShoI, addError,    addError,    addStriShoI, addRemaShoI, addCxmaShoI, addShoIShoI,  addError},
/* 10 Config data   */ {addError,    addError,    addError,    addError,    addError,    addError,    addError,    addError,    addError,     addError}
};

#if (EXTRA_INFO_ON_CALC_ERROR == 1)
  void addError(void) {
    displayCalcErrorMessage(ERROR_INVALID_DATA_TYPE_FOR_OP, ERR_REGISTER_LINE, REGISTER_X);
    errorMoreInfo("cannot add %s\nto%s",
        getRegisterDataTypeName(REGISTER_X, true, false),
        getRegisterDataTypeName(REGISTER_Y, true, false));
  }
#endif // (EXTRA_INFO_ON_CALC_ERROR == 1)



void fnAdd(uint16_t unusedButMandatoryParameter) {
  if(!saveLastX()) {
    return;
  }

  addition[getRegisterDataType(REGISTER_X)][getRegisterDataType(REGISTER_Y)]();

  adjustResult(REGISTER_X, true, true, REGISTER_X, REGISTER_Y, -1);
}



void addLonILonI(void) {
  longInteger_t y, x;

  convertLongIntegerRegisterToLongInteger(REGISTER_Y, y);
  convertLongIntegerRegisterToLongInteger(REGISTER_X, x);

  longIntegerAdd(y, x, x);

  convertLongIntegerToLongIntegerRegister(x, REGISTER_X);

  longIntegerFree(y);
  longIntegerFree(x);
}



void addLonITime(void) {
  convertLongIntegerRegisterToTimeRegister(REGISTER_Y, REGISTER_Y);
  real34Add(REGISTER_REAL34_DATA(REGISTER_Y), REGISTER_REAL34_DATA(REGISTER_X), REGISTER_REAL34_DATA(REGISTER_X));
}



void addTimeLonI(void) {
  convertLongIntegerRegisterToTimeRegister(REGISTER_X, REGISTER_X);
  real34Add(REGISTER_REAL34_DATA(REGISTER_Y), REGISTER_REAL34_DATA(REGISTER_X), REGISTER_REAL34_DATA(REGISTER_X));
}



void addLonIDate(void) {
  real34_t val;
  convertLongIntegerRegisterToReal34Register(REGISTER_Y, REGISTER_Y);
  real34Multiply(REGISTER_REAL34_DATA(REGISTER_Y), const34_86400, &val);
  real34Add(&val, REGISTER_REAL34_DATA(REGISTER_X), REGISTER_REAL34_DATA(REGISTER_X));
}



void addDateLonI(void) {
  real34_t val;
  convertLongIntegerRegisterToReal34Register(REGISTER_X, REGISTER_X);
  real34Multiply(REGISTER_REAL34_DATA(REGISTER_X), const34_86400, &val);
  reallocateRegister(REGISTER_X, dtDate, REAL34_SIZE_IN_BYTES, amNone);
  real34Add(REGISTER_REAL34_DATA(REGISTER_Y), &val, REGISTER_REAL34_DATA(REGISTER_X));
}



void addLonIShoI(void) {
  longInteger_t y, x;

  convertLongIntegerRegisterToLongInteger(REGISTER_Y, y);
  convertShortIntegerRegisterToLongInteger(REGISTER_X, x);

  longIntegerAdd(y, x, x);

  convertLongIntegerToLongIntegerRegister(x, REGISTER_X);

  longIntegerFree(y);
  longIntegerFree(x);
}



void addShoILonI(void) {
  longInteger_t y, x;

  convertShortIntegerRegisterToLongInteger(REGISTER_Y, y);
  convertLongIntegerRegisterToLongInteger(REGISTER_X, x);

  longIntegerAdd(y, x, x);

  convertLongIntegerToLongIntegerRegister(x, REGISTER_X);

  longIntegerFree(y);
  longIntegerFree(x);
}



void addLonIReal(void) {
  real_t y, x;
  angularMode_t xAngularMode;

  convertLongIntegerRegisterToReal(REGISTER_Y, &y, &ctxtReal39);
  real34ToReal(REGISTER_REAL34_DATA(REGISTER_X), &x);
  xAngularMode = getRegisterAngularMode(REGISTER_X);

  if(xAngularMode == amNone) {
    realAdd(&y, &x, &x, &ctxtReal39);
    convertRealToReal34ResultRegister(&x, REGISTER_X);
  }
  else {
    if(currentAngularMode == amMultPi) {
      realMultiply(&y, const_pi, &y, &ctxtReal39);
    }
    convertAngleFromTo(&x, xAngularMode, currentAngularMode, &ctxtReal39);
    realAdd(&y, &x, &x, &ctxtReal39);
    convertRealToReal34ResultRegister(&x, REGISTER_X);
    setRegisterAngularMode(REGISTER_X, currentAngularMode);
  }
}



void addRealLonI(void) {
  real_t y, x;
  angularMode_t yAngularMode;

  real34ToReal(REGISTER_REAL34_DATA(REGISTER_Y), &y);
  yAngularMode = getRegisterAngularMode(REGISTER_Y);
  convertLongIntegerRegisterToReal(REGISTER_X, &x, &ctxtReal39);
  reallocateRegister(REGISTER_X, dtReal34, REAL34_SIZE_IN_BYTES, amNone);

  if(yAngularMode == amNone) {
    realAdd(&y, &x, &x, &ctxtReal39);
    convertRealToReal34ResultRegister(&x, REGISTER_X);
  }
  else {
    if(currentAngularMode == amMultPi) {
      realMultiply(&x, const_pi, &x, &ctxtReal39);
    }
    convertAngleFromTo(&y, yAngularMode, currentAngularMode, &ctxtReal39);
    realAdd(&y, &x, &x, &ctxtReal39);
    convertRealToReal34ResultRegister(&x, REGISTER_X);
    setRegisterAngularMode(REGISTER_X, currentAngularMode);
  }
}



void addLonICplx(void) {
  real_t a, c;

  convertLongIntegerRegisterToReal(REGISTER_Y, &a, &ctxtReal39);
  real34ToReal(REGISTER_REAL34_DATA(REGISTER_X), &c);

  realAdd(&a, &c, &c, &ctxtReal39);

  convertRealToReal34ResultRegister(&c, REGISTER_X);
}



void addCplxLonI(void) {
  real_t a, c;
  real34_t b;

  real34ToReal(REGISTER_REAL34_DATA(REGISTER_Y), &a);
  real34Copy(REGISTER_IMAG34_DATA(REGISTER_Y), &b);
  convertLongIntegerRegisterToReal(REGISTER_X, &c, &ctxtReal39);

  realAdd(&a, &c, &c, &ctxtReal39);

  reallocateRegister(REGISTER_X, dtComplex34, COMPLEX34_SIZE_IN_BYTES, amNone);
  convertRealToReal34ResultRegister(&c, REGISTER_X);
  real34Copy(&b, REGISTER_IMAG34_DATA(REGISTER_X));
}



void addTimeTime(void) {
  real34Add(REGISTER_REAL34_DATA(REGISTER_Y), REGISTER_REAL34_DATA(REGISTER_X), REGISTER_REAL34_DATA(REGISTER_X));
}



void addTimeReal(void) {
  angularMode_t xAngularMode;

  xAngularMode = getRegisterAngularMode(REGISTER_X);

  if(xAngularMode == amNone) {
    convertReal34RegisterToTimeRegister(REGISTER_X, REGISTER_X);
    real34Add(REGISTER_REAL34_DATA(REGISTER_Y), REGISTER_REAL34_DATA(REGISTER_X), REGISTER_REAL34_DATA(REGISTER_X));
  }
  else {
    addError();
  }
}



void addRealTime(void) {
  angularMode_t yAngularMode;

  yAngularMode = getRegisterAngularMode(REGISTER_Y);

  if(yAngularMode == amNone) {
    convertReal34RegisterToTimeRegister(REGISTER_Y, REGISTER_Y);
    real34Add(REGISTER_REAL34_DATA(REGISTER_Y), REGISTER_REAL34_DATA(REGISTER_X), REGISTER_REAL34_DATA(REGISTER_X));
  }
  else {
    addError();
  }
}



void addDateReal(void) {
  angularMode_t xAngularMode;
  real34_t val;

  xAngularMode = getRegisterAngularMode(REGISTER_X);

  if(xAngularMode == amNone) {
    real34ToIntegralValue(REGISTER_REAL34_DATA(REGISTER_X), REGISTER_REAL34_DATA(REGISTER_X), roundingModeTable[roundingMode]);
    real34Multiply(REGISTER_REAL34_DATA(REGISTER_X), const34_86400, &val);
    reallocateRegister(REGISTER_X, dtDate, REAL34_SIZE_IN_BYTES, amNone);
    real34Add(REGISTER_REAL34_DATA(REGISTER_Y), &val, REGISTER_REAL34_DATA(REGISTER_X));
  }
  else {
    addError();
  }
}



void addRealDate(void) {
  angularMode_t yAngularMode;
  real34_t val;

  yAngularMode = getRegisterAngularMode(REGISTER_Y);

  if(yAngularMode == amNone) {
    real34ToIntegralValue(REGISTER_REAL34_DATA(REGISTER_Y), REGISTER_REAL34_DATA(REGISTER_Y), roundingModeTable[roundingMode]);
    real34Multiply(REGISTER_REAL34_DATA(REGISTER_Y), const34_86400, &val);
    real34Add(&val, REGISTER_REAL34_DATA(REGISTER_X), REGISTER_REAL34_DATA(REGISTER_X));
  }
  else {
    addError();
  }
}



static void _addString(const char *stringToAppend) {
  int16_t len1, len2;

  if(stringGlyphLength(REGISTER_STRING_DATA(REGISTER_Y)) + stringGlyphLength(stringToAppend) > MAX_NUMBER_OF_GLYPHS_IN_STRING) {
    displayCalcErrorMessage(ERROR_STRING_WOULD_BE_TOO_LONG, ERR_REGISTER_LINE, REGISTER_X);
    errorMoreInfo("the resulting string would be %d (Y %d + X %d) characters long. Maximum is %d",
        stringGlyphLength(REGISTER_STRING_DATA(REGISTER_Y)) + stringGlyphLength(stringToAppend),
        stringGlyphLength(REGISTER_STRING_DATA(REGISTER_Y)),
        stringGlyphLength(stringToAppend), MAX_NUMBER_OF_GLYPHS_IN_STRING);
  }
  else {
    len1 = stringByteLength(REGISTER_STRING_DATA(REGISTER_Y));
    len2 = stringByteLength(stringToAppend) + 1;

    reallocateRegister(REGISTER_X, dtString, len1 + len2, amNone);

    xcopy(REGISTER_STRING_DATA(REGISTER_X),        REGISTER_STRING_DATA(REGISTER_Y), len1);
    xcopy(REGISTER_STRING_DATA(REGISTER_X) + len1, stringToAppend,                   len2);
  }
}

static void _addPrecedingString(const char *stringToAppend) {
  int16_t len1, len2;

  if(stringGlyphLength(REGISTER_STRING_DATA(REGISTER_X)) + stringGlyphLength(stringToAppend) > MAX_NUMBER_OF_GLYPHS_IN_STRING) {
    displayCalcErrorMessage(ERROR_STRING_WOULD_BE_TOO_LONG, ERR_REGISTER_LINE, REGISTER_X);
    errorMoreInfo("the resulting string would be %d (Y %d + X %d) characters long. Maximum is %d",
        stringGlyphLength(REGISTER_STRING_DATA(REGISTER_X)) + stringGlyphLength(stringToAppend),
        stringGlyphLength(stringToAppend),
        stringGlyphLength(REGISTER_STRING_DATA(REGISTER_X)), MAX_NUMBER_OF_GLYPHS_IN_STRING);
  }
  else {
    len1 = stringByteLength(stringToAppend);
    len2 = stringByteLength(REGISTER_STRING_DATA(REGISTER_X)) + 1;

    fnSwapXY(NOPARAM);
    reallocateRegister(REGISTER_X, dtString, len1 + len2, amNone);

    xcopy(REGISTER_STRING_DATA(REGISTER_X),        stringToAppend,                   len1);
    xcopy(REGISTER_STRING_DATA(REGISTER_X) + len1, REGISTER_STRING_DATA(REGISTER_Y), len2);
  }
}



void addStriLonI(void) {
  longIntegerRegisterToDisplayString(REGISTER_X, tmpString, TMP_STR_LENGTH, SCREEN_WIDTH, 50, STD_SPACE_PUNCTUATION);
  _addString(tmpString);
}



void addLonIStri(void) {
  longIntegerRegisterToDisplayString(REGISTER_Y, tmpString, TMP_STR_LENGTH, SCREEN_WIDTH, 50, STD_SPACE_PUNCTUATION);
  _addPrecedingString(tmpString);
}



void addStriTime(void) {
  timeToDisplayString(REGISTER_X, tmpString, false);
  _addString(tmpString);
}



void addTimeStri(void) {
  timeToDisplayString(REGISTER_Y, tmpString, false);
  _addPrecedingString(tmpString);
}



void addStriDate(void) {
  dateToDisplayString(REGISTER_X, tmpString);
  _addString(tmpString);
}



void addDateStri(void) {
  dateToDisplayString(REGISTER_Y, tmpString);
  _addPrecedingString(tmpString);
}



void addStriStri(void) {
  xcopy(tmpString, REGISTER_STRING_DATA(REGISTER_X), stringByteLength(REGISTER_STRING_DATA(REGISTER_X)) + 1);
  _addString(tmpString);
}



void addStriRema(void) {
  real34MatrixToDisplayString(REGISTER_X, tmpString);
  _addString(tmpString);
}



void addRemaStri(void) {
  real34MatrixToDisplayString(REGISTER_Y, tmpString);
  _addPrecedingString(tmpString);
}



void addStriCxma(void) {
  complex34MatrixToDisplayString(REGISTER_X, tmpString);
  _addString(tmpString);
}



void addCxmaStri(void) {
  complex34MatrixToDisplayString(REGISTER_Y, tmpString);
  _addPrecedingString(tmpString);
}



void addStriShoI(void) {
  shortIntegerToDisplayString(REGISTER_X, tmpString, false);
  _addString(tmpString);
}



void addShoIStri(void) {
  shortIntegerToDisplayString(REGISTER_Y, tmpString, false);
  _addPrecedingString(tmpString);
}



void addStriReal(void) {
  real34ToDisplayString(REGISTER_REAL34_DATA(REGISTER_X), getRegisterAngularMode(REGISTER_X), tmpString, &standardFont, SCREEN_WIDTH, NUMBER_OF_DISPLAY_DIGITS, false, STD_SPACE_PUNCTUATION, true);
  _addString(tmpString);
}



void addRealStri(void) {
  real34ToDisplayString(REGISTER_REAL34_DATA(REGISTER_Y), getRegisterAngularMode(REGISTER_Y), tmpString, &standardFont, SCREEN_WIDTH, NUMBER_OF_DISPLAY_DIGITS, false, STD_SPACE_PUNCTUATION, true);
  _addPrecedingString(tmpString);
}



void addStriCplx(void) {
  complex34ToDisplayString(REGISTER_COMPLEX34_DATA(REGISTER_X), tmpString, &numericFont, SCREEN_WIDTH, NUMBER_OF_DISPLAY_DIGITS, false, STD_SPACE_PUNCTUATION, true);
  _addString(tmpString);
}



void addCplxStri(void) {
  complex34ToDisplayString(REGISTER_COMPLEX34_DATA(REGISTER_Y), tmpString, &numericFont, SCREEN_WIDTH, NUMBER_OF_DISPLAY_DIGITS, false, STD_SPACE_PUNCTUATION, true);
  _addPrecedingString(tmpString);
}



void addRemaLonI(void) {
  real34Matrix_t ym;
  real_t y, x;

  linkToRealMatrixRegister(REGISTER_Y, &ym);

  const uint16_t rows = ym.header.matrixRows;
  const uint16_t cols = ym.header.matrixColumns;
  int32_t i;

  convertLongIntegerRegisterToReal(REGISTER_X, &x, &ctxtReal39);
  for(i = 0; i < cols * rows; ++i) {
    real34ToReal(&ym.matrixElements[i], &y);
    realAdd(&y, &x, &y, &ctxtReal39);
    roundToSignificantDigits(&y, &y, significantDigits == 0 ? 34 : significantDigits, &ctxtReal39);
    realToReal34(&y, &ym.matrixElements[i]);
  }
  fnSwapXY(NOPARAM);
}



void addLonIRema(void) {
  real34Matrix_t xm;
  real_t y, x;

  linkToRealMatrixRegister(REGISTER_X, &xm);

  const uint16_t rows = xm.header.matrixRows;
  const uint16_t cols = xm.header.matrixColumns;
  int32_t i;

  convertLongIntegerRegisterToReal(REGISTER_Y, &y, &ctxtReal39);
  for(i = 0; i < cols * rows; ++i) {
    real34ToReal(&xm.matrixElements[i], &x);
    realAdd(&y, &x, &x, &ctxtReal39);
    roundToSignificantDigits(&x, &x, significantDigits == 0 ? 34 : significantDigits, &ctxtReal39);
    realToReal34(&x, &xm.matrixElements[i]);
  }
}



void addRemaRema(void) {
  real34Matrix_t y, x;

  linkToRealMatrixRegister(REGISTER_Y, &y);
  convertReal34MatrixRegisterToReal34Matrix(REGISTER_X, &x);

  addRealMatrices(&y, &x, &x);
  if(x.matrixElements) {
    convertReal34MatrixToReal34MatrixRegister(&x, REGISTER_X);
  }
  else {
    displayCalcErrorMessage(ERROR_MATRIX_MISMATCH, ERR_REGISTER_LINE, REGISTER_X);
    errorMoreInfo("cannot add %d" STD_CROSS "%d-matrix to %d" STD_CROSS "%d-matrix",
        x.header.matrixRows, x.header.matrixColumns,
        y.header.matrixRows, y.header.matrixColumns);
  }

  realMatrixFree(&x);
}



void addRemaCxma(void) {
  convertReal34MatrixRegisterToComplex34MatrixRegister(REGISTER_Y, REGISTER_Y);
  addCxmaCxma();
}



void addCxmaRema(void) {
  convertReal34MatrixRegisterToComplex34MatrixRegister(REGISTER_X, REGISTER_X);
  addCxmaCxma();
}



void addRemaShoI(void) {
  real34Matrix_t ym;
  real_t y, x;

  linkToRealMatrixRegister(REGISTER_Y, &ym);

  const uint16_t rows = ym.header.matrixRows;
  const uint16_t cols = ym.header.matrixColumns;
  int32_t i;

  convertShortIntegerRegisterToReal(REGISTER_X, &x, &ctxtReal39);
  for(i = 0; i < cols * rows; ++i) {
    real34ToReal(&ym.matrixElements[i], &y);
    realAdd(&y, &x, &y, &ctxtReal39);
    roundToSignificantDigits(&y, &y, significantDigits == 0 ? 34 : significantDigits, &ctxtReal39);
    realToReal34(&y, &ym.matrixElements[i]);
  }
  fnSwapXY(NOPARAM);
}



void addShoIRema(void) {
  real34Matrix_t xm;
  real_t y, x;

  linkToRealMatrixRegister(REGISTER_X, &xm);

  const uint16_t rows = xm.header.matrixRows;
  const uint16_t cols = xm.header.matrixColumns;
  int32_t i;

  convertShortIntegerRegisterToReal(REGISTER_Y, &y, &ctxtReal39);
  for(i = 0; i < cols * rows; ++i) {
    real34ToReal(&xm.matrixElements[i], &x);
    realAdd(&y, &x, &x, &ctxtReal39);
    roundToSignificantDigits(&x, &x, significantDigits == 0 ? 34 : significantDigits, &ctxtReal39);
    realToReal34(&x, &xm.matrixElements[i]);
  }
}



void addRemaReal(void) {
  real34Matrix_t y;
  angularMode_t xAngularMode;

  linkToRealMatrixRegister(REGISTER_Y, &y);
  xAngularMode = getRegisterAngularMode(REGISTER_X);

  if(xAngularMode == amNone) {
    const uint16_t rows = y.header.matrixRows;
    const uint16_t cols = y.header.matrixColumns;
    int32_t i;

    for(i = 0; i < cols * rows; ++i) {
      real34Add(&y.matrixElements[i], REGISTER_REAL34_DATA(REGISTER_X), &y.matrixElements[i]);
    }
    fnSwapXY(NOPARAM);
  }
  else {
    elementwiseRemaReal(addRealReal);
  }
}



void addRealRema(void) {
  real34Matrix_t x;
  angularMode_t yAngularMode;

  linkToRealMatrixRegister(REGISTER_X, &x);
  yAngularMode = getRegisterAngularMode(REGISTER_Y);

  if(yAngularMode == amNone) {
    const uint16_t rows = x.header.matrixRows;
    const uint16_t cols = x.header.matrixColumns;
    int32_t i;

    for(i = 0; i < cols * rows; ++i) {
      real34Add(REGISTER_REAL34_DATA(REGISTER_Y), &x.matrixElements[i], &x.matrixElements[i]);
    }
  }
  else {
    elementwiseRealRema(addRealReal);
  }
}



void addRemaCplx(void) {
  convertReal34MatrixRegisterToComplex34MatrixRegister(REGISTER_Y, REGISTER_Y);
  addCxmaCplx();
}



void addCplxRema(void) {
  convertReal34MatrixRegisterToComplex34MatrixRegister(REGISTER_X, REGISTER_X);
  addCplxCxma();
}



void addCxmaLonI(void) {
  complex34Matrix_t ym;
  real_t y, x;

  linkToComplexMatrixRegister(REGISTER_Y, &ym);

  const uint16_t rows = ym.header.matrixRows;
  const uint16_t cols = ym.header.matrixColumns;
  int32_t i;

  convertLongIntegerRegisterToReal(REGISTER_X, &x, &ctxtReal39);
  for(i = 0; i < cols * rows; ++i) {
    real34ToReal(VARIABLE_REAL34_DATA(&ym.matrixElements[i]), &y);
    realAdd(&y, &x, &y, &ctxtReal39);
    roundToSignificantDigits(&y, &y, significantDigits == 0 ? 34 : significantDigits, &ctxtReal39);
    realToReal34(&y, VARIABLE_REAL34_DATA(&ym.matrixElements[i]));
  }
  fnSwapXY(NOPARAM);
}



void addLonICxma(void) {
  complex34Matrix_t xm;
  real_t y, x;

  linkToComplexMatrixRegister(REGISTER_X, &xm);

  const uint16_t rows = xm.header.matrixRows;
  const uint16_t cols = xm.header.matrixColumns;
  int32_t i;

  convertLongIntegerRegisterToReal(REGISTER_Y, &y, &ctxtReal39);
  for(i = 0; i < cols * rows; ++i) {
    real34ToReal(VARIABLE_REAL34_DATA(&xm.matrixElements[i]), &x);
    realAdd(&y, &x, &x, &ctxtReal39);
    roundToSignificantDigits(&x, &x, significantDigits == 0 ? 34 : significantDigits, &ctxtReal39);
    realToReal34(&x, VARIABLE_REAL34_DATA(&xm.matrixElements[i]));
  }
}



void addCxmaCxma(void) {
  complex34Matrix_t y, x;

  linkToComplexMatrixRegister(REGISTER_Y, &y);
  convertComplex34MatrixRegisterToComplex34Matrix(REGISTER_X, &x);

  addComplexMatrices(&y, &x, &x);
  if(x.matrixElements) {
    convertComplex34MatrixToComplex34MatrixRegister(&x, REGISTER_X);
  }
  else {
    displayCalcErrorMessage(ERROR_MATRIX_MISMATCH, ERR_REGISTER_LINE, REGISTER_X);
    errorMoreInfo("cannot add %d" STD_CROSS "%d-matrix to %d" STD_CROSS "%d-matrix",
        x.header.matrixRows, x.header.matrixColumns,
        y.header.matrixRows, y.header.matrixColumns);
  }

  complexMatrixFree(&x);
}



void addCxmaShoI(void) {
  complex34Matrix_t ym;
  real_t y, x;

  linkToComplexMatrixRegister(REGISTER_Y, &ym);

  const uint16_t rows = ym.header.matrixRows;
  const uint16_t cols = ym.header.matrixColumns;
  int32_t i;

  convertShortIntegerRegisterToReal(REGISTER_X, &x, &ctxtReal39);
  for(i = 0; i < cols * rows; ++i) {
    real34ToReal(VARIABLE_REAL34_DATA(&ym.matrixElements[i]), &y);
    realAdd(&y, &x, &y, &ctxtReal39);
    roundToSignificantDigits(&y, &y, significantDigits == 0 ? 34 : significantDigits, &ctxtReal39);
    realToReal34(&y, VARIABLE_REAL34_DATA(&ym.matrixElements[i]));
  }
  fnSwapXY(NOPARAM);
}



void addShoICxma(void) {
  complex34Matrix_t xm;
  real_t y, x;

  linkToComplexMatrixRegister(REGISTER_X, &xm);

  const uint16_t rows = xm.header.matrixRows;
  const uint16_t cols = xm.header.matrixColumns;
  int32_t i;

  convertShortIntegerRegisterToReal(REGISTER_Y, &y, &ctxtReal39);
  for(i = 0; i < cols * rows; ++i) {
    real34ToReal(VARIABLE_REAL34_DATA(&xm.matrixElements[i]), &x);
    realAdd(&y, &x, &x, &ctxtReal39);
    roundToSignificantDigits(&x, &x, significantDigits == 0 ? 34 : significantDigits, &ctxtReal39);
    realToReal34(&x, VARIABLE_REAL34_DATA(&xm.matrixElements[i]));
  }
}



void addCxmaReal(void) {
  complex34Matrix_t y;
  angularMode_t xAngularMode;

  linkToComplexMatrixRegister(REGISTER_Y, &y);
  xAngularMode = getRegisterAngularMode(REGISTER_X);

  if(xAngularMode == amNone) {
    const uint16_t rows = y.header.matrixRows;
    const uint16_t cols = y.header.matrixColumns;
    int32_t i;

    for(i = 0; i < cols * rows; ++i) {
      real34Add(VARIABLE_REAL34_DATA(&y.matrixElements[i]), REGISTER_REAL34_DATA(REGISTER_X), VARIABLE_REAL34_DATA(&y.matrixElements[i]));
    }
    fnSwapXY(NOPARAM);
  }
  else {
    elementwiseCxmaReal(addCplxReal);
  }
}



void addRealCxma(void) {
  complex34Matrix_t x;
  angularMode_t yAngularMode;

  linkToComplexMatrixRegister(REGISTER_X, &x);
  yAngularMode = getRegisterAngularMode(REGISTER_Y);

  if(yAngularMode == amNone) {
    const uint16_t rows = x.header.matrixRows;
    const uint16_t cols = x.header.matrixColumns;
    int32_t i;

    for(i = 0; i < cols * rows; ++i) {
      real34Add(REGISTER_REAL34_DATA(REGISTER_Y), VARIABLE_REAL34_DATA(&x.matrixElements[i]), VARIABLE_REAL34_DATA(&x.matrixElements[i]));
    }
  }
  else {
    elementwiseRealCxma(addRealCplx);
  }
}



void addCxmaCplx(void) {
  complex34Matrix_t y;

  linkToComplexMatrixRegister(REGISTER_Y, &y);

  const uint16_t rows = y.header.matrixRows;
  const uint16_t cols = y.header.matrixColumns;
  int32_t i;

  for(i = 0; i < cols * rows; ++i) {
    real34Add(VARIABLE_REAL34_DATA(&y.matrixElements[i]), REGISTER_REAL34_DATA(REGISTER_X), VARIABLE_REAL34_DATA(&y.matrixElements[i]));
    real34Add(VARIABLE_IMAG34_DATA(&y.matrixElements[i]), REGISTER_IMAG34_DATA(REGISTER_X), VARIABLE_IMAG34_DATA(&y.matrixElements[i]));
  }
  fnSwapXY(NOPARAM);
}



void addCplxCxma(void) {
  complex34Matrix_t x;

  linkToComplexMatrixRegister(REGISTER_X, &x);

  const uint16_t rows = x.header.matrixRows;
  const uint16_t cols = x.header.matrixColumns;
  int32_t i;

  for(i = 0; i < cols * rows; ++i) {
    real34Add(REGISTER_REAL34_DATA(REGISTER_Y), VARIABLE_REAL34_DATA(&x.matrixElements[i]), VARIABLE_REAL34_DATA(&x.matrixElements[i]));
    real34Add(REGISTER_IMAG34_DATA(REGISTER_Y), VARIABLE_IMAG34_DATA(&x.matrixElements[i]), VARIABLE_IMAG34_DATA(&x.matrixElements[i]));
  }
}



void addShoIShoI(void) {
  setRegisterTag(REGISTER_X, getRegisterTag(REGISTER_Y));
  *(REGISTER_SHORT_INTEGER_DATA(REGISTER_X)) = WP34S_intAdd(*(REGISTER_SHORT_INTEGER_DATA(REGISTER_Y)), *(REGISTER_SHORT_INTEGER_DATA(REGISTER_X)));
}



void addShoIReal(void) {
  real_t y, x;
  angularMode_t xAngularMode;

  convertShortIntegerRegisterToReal(REGISTER_Y, &y, &ctxtReal39);
  real34ToReal(REGISTER_REAL34_DATA(REGISTER_X), &x);
  xAngularMode = getRegisterAngularMode(REGISTER_X);

  if(xAngularMode == amNone) {
    realAdd(&y, &x, &x, &ctxtReal39);
    convertRealToReal34ResultRegister(&x, REGISTER_X);
  }
  else {
    if(currentAngularMode == amMultPi) {
      realMultiply(&y, const_pi, &y, &ctxtReal39);
    }
    convertAngleFromTo(&x, xAngularMode, currentAngularMode, &ctxtReal39);
    realAdd(&y, &x, &x, &ctxtReal39);
    convertRealToReal34ResultRegister(&x, REGISTER_X);
    setRegisterAngularMode(REGISTER_X, currentAngularMode);
  }
}



void addRealShoI(void) {
  real_t y, x;
  angularMode_t yAngularMode;

  real34ToReal(REGISTER_REAL34_DATA(REGISTER_Y), &y);
  yAngularMode = getRegisterAngularMode(REGISTER_Y);
  convertShortIntegerRegisterToReal(REGISTER_X, &x, &ctxtReal39);
  reallocateRegister(REGISTER_X, dtReal34, REAL34_SIZE_IN_BYTES, amNone);

  if(yAngularMode == amNone) {
    realAdd(&y, &x, &x, &ctxtReal39);
    convertRealToReal34ResultRegister(&x, REGISTER_X);
  }
  else {
    if(currentAngularMode == amMultPi) {
      realMultiply(&x, const_pi, &x, &ctxtReal39);
    }
    convertAngleFromTo(&y, yAngularMode, currentAngularMode, &ctxtReal39);
    realAdd(&y, &x, &x, &ctxtReal39);
    convertRealToReal34ResultRegister(&x, REGISTER_X);
    setRegisterAngularMode(REGISTER_X, currentAngularMode);
  }
}



void addShoICplx(void) {
  convertShortIntegerRegisterToReal34Register(REGISTER_Y, REGISTER_Y);
  real34Add(REGISTER_REAL34_DATA(REGISTER_Y), REGISTER_REAL34_DATA(REGISTER_X), REGISTER_REAL34_DATA(REGISTER_X)); // real part
}



void addCplxShoI(void) {
  convertShortIntegerRegisterToReal34Register(REGISTER_X, REGISTER_X);
  real34Add(REGISTER_REAL34_DATA(REGISTER_Y), REGISTER_REAL34_DATA(REGISTER_X), REGISTER_REAL34_DATA(REGISTER_Y)); // real part
  reallocateRegister(REGISTER_X, dtComplex34, COMPLEX34_SIZE_IN_BYTES, amNone);
  complex34Copy(REGISTER_COMPLEX34_DATA(REGISTER_Y), REGISTER_COMPLEX34_DATA(REGISTER_X));
}



void addRealReal(void) {
  angularMode_t yAngularMode, xAngularMode;

  yAngularMode = getRegisterAngularMode(REGISTER_Y);
  xAngularMode = getRegisterAngularMode(REGISTER_X);

  if(yAngularMode == amNone && xAngularMode == amNone) {
    real34Add(REGISTER_REAL34_DATA(REGISTER_Y), REGISTER_REAL34_DATA(REGISTER_X), REGISTER_REAL34_DATA(REGISTER_X));
  }
  else {
    real_t y, x;

    real34ToReal(REGISTER_REAL34_DATA(REGISTER_Y), &y);
    real34ToReal(REGISTER_REAL34_DATA(REGISTER_X), &x);

    if(yAngularMode == amNone) {
      yAngularMode = currentAngularMode;
      if(currentAngularMode == amMultPi) {
        realMultiply(&y, const_pi, &y, &ctxtReal39);
      }
    }
    else if(xAngularMode == amNone) {
      xAngularMode = currentAngularMode;
      if(currentAngularMode == amMultPi) {
        realMultiply(&x, const_pi, &x, &ctxtReal39);
      }
    }

    convertAngleFromTo(&y, yAngularMode, currentAngularMode, &ctxtReal39);
    convertAngleFromTo(&x, xAngularMode, currentAngularMode, &ctxtReal39);

    realAdd(&y, &x, &x, &ctxtReal39);
    convertRealToReal34ResultRegister(&x, REGISTER_X);
    setRegisterAngularMode(REGISTER_X, currentAngularMode);
  }
}



void addRealCplx(void) {
  real34Add(REGISTER_REAL34_DATA(REGISTER_Y), REGISTER_REAL34_DATA(REGISTER_X), REGISTER_REAL34_DATA(REGISTER_X)); // real part
}



void addCplxReal(void) {
  real34Add(REGISTER_REAL34_DATA(REGISTER_Y), REGISTER_REAL34_DATA(REGISTER_X), REGISTER_REAL34_DATA(REGISTER_Y)); // real part
  reallocateRegister(REGISTER_X, dtComplex34, COMPLEX34_SIZE_IN_BYTES, amNone);
  complex34Copy(REGISTER_COMPLEX34_DATA(REGISTER_Y), REGISTER_COMPLEX34_DATA(REGISTER_X)); // imaginary part
}



void addCplxCplx(void) {
  real34Add(REGISTER_REAL34_DATA(REGISTER_Y), REGISTER_REAL34_DATA(REGISTER_X), REGISTER_REAL34_DATA(REGISTER_X)); // real part
  real34Add(REGISTER_IMAG34_DATA(REGISTER_Y), REGISTER_IMAG34_DATA(REGISTER_X), REGISTER_IMAG34_DATA(REGISTER_X)); // imaginary part
}
