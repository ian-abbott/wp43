WP43 Code Documentation
========================

Firmware for the WP43 pocket calculator: a super-set of the legendary HP-42S RPN
Scientific. These pages document the code for the firmware. Other documents and
resources are available:

* The `GitLab home <https://gitlab.com/wpcalculators/wp43>`_ for the project is where the source code, continuous integration, releases, and wiki can be found.
* `Owner's Manual and Reference Manual <https://gitlab.com/wpcalculators/wp43/-/releases>`_ for users of the calculator.
* The `GitLab Wiki <https://gitlab.com/wpcalculators/wp43/-/wikis/home>`_ describes the development process and how to build the calculator firmware.

Contents
--------

.. toctree::
   :maxdepth: 2
   :caption: Hardware Abstraction Layer

   audio
   gui
   io
   lcd
   system
   time
   timer

.. toctree::
   :maxdepth: 2
   :caption: Core

   freeList

.. toctree::
   :maxdepth: 2
   :caption: User Interface

   tam

.. toctree::
   :maxdepth: 2
   :caption: Appendices

   genindex
